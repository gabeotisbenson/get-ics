import config from './rollup.config.shared.js';
import html from '@rollup/plugin-html';
import serve from 'rollup-plugin-serve';

export default {
	...config,
	plugins: [
		...config.plugins,
		html(),
		serve({
			contentBase: 'dist',
			port: 1234
		})
	]
};
