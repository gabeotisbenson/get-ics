import alias from '@rollup/plugin-alias';
import babel from 'rollup-plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import del from 'rollup-plugin-delete';
import { eslint } from 'rollup-plugin-eslint';
import json from 'rollup-plugin-json';
import { name } from './package.json';
import path from 'path';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import visualizer from 'rollup-plugin-visualizer';

const fileTypes = ['.js', '.json'];

export default {
	input: './src/index.js',
	output: {
		name: 'ICS',
		file: 'dist/get-ics.js',
		format: 'umd',
		sourcemap: true
	},
	plugins: [
		del({ targets: ['dist/*', 'stats.html'] }),
		commonjs({
			namedExports: {
				'@gabegabegabe/utils/dist/array/reducers': ['sum'],
				'file-saver': ['saveAs']
			}
		}),
		json({ preferConst: true, compact: true }),
		alias({
			resolve: fileTypes,
			entries: [
				{
					find: /^~\/(?<module>.*)/u,
					replacement: path.resolve(__dirname, 'src', '$1')
				}
			]
		}),
		resolve({ browser: true, extensions: fileTypes }),
		eslint({ include: ['src/**/*.js'] }),
		babel(),
		terser(),
		visualizer({
			filename: './STATS.html',
			sourcemap: true,
			title: `${name} Bundle Size Statistics`
		})
	]
};
