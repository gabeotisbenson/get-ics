/* global BlobBuilder */
import dateFormat from 'dateformat';
import { saveAs } from 'file-saver';
import { sum } from '@gabegabegabe/utils/dist/array/reducers';

const BYDAY_VALUES = [
	'SU',
	'MO',
	'TU',
	'WE',
	'TH',
	'FR',
	'SA'
];
const DAYS_IN_A_WEEK = 7;
const IS_IE = navigator.userAgent.indexOf('MSIE') >= 0;
const IS_OUDATED_IE = IS_IE && navigator.userAgent.indexOf('MSIE 10') < 0;
const SEPARATOR = navigator.appVersion.indexOf('Win') === -1 ? '\n' : '\r\n';
const VALID_FREQUENCIES = [
	'yearly',
	'monthly',
	'weekly',
	'daily'
];

const validateRepeatOptions = ({ freq, until, count, interval, byday }) => {
	if (VALID_FREQUENCIES.indexOf(freq) === -1) throw new Error(`Invalid repeat frequency, must be one of the following: ${VALID_FREQUENCIES.join(', ')}`);
	if (until && isNaN(Date.parse(until))) throw new Error('Repeat until must be a valid date string');
	if (interval && isNaN(parseInt(interval))) throw new Error('Repeat interval must be an integer');
	if (count && isNaN(parseInt(count))) throw new Error('Repeat count must be an integer');

	if (!byday) return;

	if (!Array.isArray(byday)) throw new Error('Repeat byday must be an array');
	if (byday.length > DAYS_IN_A_WEEK) throw new Error(`Repeat byday array must be no longer than the ${DAYS_IN_A_WEEK} days in a week`);

	const dedupedByday = byday.filter((day, i, arr) => arr.indexOf(day) === i);

	for (const i in dedupedByday) if (BYDAY_VALUES.indexOf(dedupedByday[i]) < 0) throw new Error(`Repeat byday values must include only the following: ${BYDAY_VALUES.join(', ')}`);
};

const parseDate = date => ({
	year: dateFormat(date, 'yyyy'),
	month: dateFormat(date, 'mm'),
	day: dateFormat(date, 'dd'),
	hour: dateFormat(date, 'HH'),
	minute: dateFormat(date, 'MM'),
	second: dateFormat(date, 'ss')
});

class ics {
	constructor (uidDomain = 'default', prodId = 'Calendar') {
		if (IS_OUDATED_IE) throw new Error('Unsupported browser');
		this.calendarEvents = [];
		this.calendarStart = ['BEGIN:VCALENDAR', `PRODID:${prodId}`, 'VERSION:2.0']
			.join(SEPARATOR);
		this.calendarEnd = 'END:VCALENDAR';
		this.uidDomain = uidDomain;
		this.prodId = prodId;
	}

	get calendar () {
		return [this.calendarStart, ...this.calendarEvents, this.calendarEnd]
			.join(SEPARATOR);
	}

	get events () { return this.calendarEvents; }

	addEvent ({
		subject,
		description,
		location,
		begin,
		end,
		repeat
	}) {
		const requiredArgs = [
			subject,
			description,
			location,
			begin,
			end
		];
		for (const i in requiredArgs) if (!requiredArgs[i]) return false;

		if (repeat) validateRepeatOptions(repeat);

		const startDate = new Date(begin);
		const endDate = new Date(end);
		const nowDate = new Date();

		const {
			year: startYear,
			month: startMonth,
			day: startDay,
			hour: startHour,
			minute: startMinute,
			second: startSecond
		} = parseDate(startDate);

		const {
			year: endYear,
			month: endMonth,
			day: endDay,
			hour: endHour,
			minute: endMinute,
			second: endSecond
		} = parseDate(endDate);

		const {
			year: nowYear,
			month: nowMonth,
			day: nowDay,
			hour: nowHour,
			minute: nowMinute,
			second: nowSecond
		} = parseDate(nowDate);

		let startTime = `T${startHour}${startMinute}${startSecond}`;
		let endTime = `T${endHour}${endMinute}${endSecond}`;
		const nowTime = `T${nowHour}${nowMinute}${nowSecond}`;
		const duration = [
			startHour,
			startMinute,
			startSecond,
			endHour,
			endMinute,
			endSecond
		]
			.map(n => Number(n))
			.reduce(sum);

		// Since some calendars don't add 0 second events, we need to remove time if there is none...
		if (duration === 0) {
			startTime = '';
			endTime = '';
		}

		const start = startYear + startMonth + startDay + startTime;
		const stop = endYear + endMonth + endDay + endTime;
		const now = nowYear + nowMonth + nowDay + nowTime;

		// Recurrence rrule lets
		let repeatRule = '';
		if (repeat) {
			repeatRule = `rrule:FREQ=${repeat.freq}`;

			if (repeat.until) {
				const uDate = new Date(Date.parse(repeat.until)).toISOString();
				// eslint-disable-next-line no-magic-numbers
				repeatRule += `;UNTIL=${uDate.substring(0, uDate.length - 13).replace(/[-]/gu, '')}000000Z`;
			}

			if (repeat.interval) repeatRule += `;INTERVAL=${repeat.interval}`;
			if (repeat.count) repeatRule += `;COUNT=${repeat.count}`;
			if (repeat.byday) repeatRule += `;BYDAY=${repeat.byday.join(',')}`;
		}

		const calendarEvent = [
			'BEGIN:VEVENT',
			`UID:${this.calendarEvents.length}@${this.uidDomain}`,
			'CLASS:PUBLIC',
			`DESCRIPTION:${description}`,
			`DTSTAMP;VALUE=DATE-TIME:${now}`,
			`DTSTART;VALUE=DATE-TIME:${start}`,
			`DTEND;VALUE=DATE-TIME:${stop}`,
			`LOCATION:${location}`,
			`SUMMARY;LANGUAGE=en-us:${subject}`,
			'TRANSP:TRANSPARENT',
			'END:VEVENT'
		];

		// eslint-disable-next-line no-magic-numbers
		if (repeatRule) calendarEvent.splice(4, 0, repeatRule);

		this.calendarEvents.push(calendarEvent.join(SEPARATOR));

		return calendarEvent;
	}

	download (filename = 'calendar', ext = '.ics') {
		if (this.calendarEvents.length < 1) return false;

		let blob = null;

		if (IS_IE) {
			const blobBuilder = new BlobBuilder();
			blobBuilder.append(this.calendar);
			blob = blobBuilder.getBlob(`text/x-vCalendar;charset=${document.characterSet}`);
		} else {
			blob = new Blob([this.calendar]);
		}

		if (blob) saveAs(blob, [filename, ext].join(''));

		return this.calendar;
	}
}

export default ics;
